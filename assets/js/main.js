window.onload = function() {
  countDownToTime("Feb 18, 2021 00:00:00", 'counter__countdown');
}
function countDownToTime(countTo, id) {
  countTo = new Date(countTo).getTime();
  var now = new Date(),
      countTo = new Date(countTo),
      timeDifference = (countTo - now);
      
  var secondsInADay = 60 * 60 * 1000 * 24,
      secondsInAHour = 60 * 60 * 1000;

  days = Math.floor(timeDifference / (secondsInADay) * 1);
  hours = Math.floor((timeDifference % (secondsInADay)) / (secondsInAHour) * 1);
  mins = Math.floor(((timeDifference % (secondsInADay)) % (secondsInAHour)) / (60 * 1000) * 1);
  secs = Math.floor((((timeDifference % (secondsInADay)) % (secondsInAHour)) % (60 * 1000)) / 1000 * 1);

  if( days < 10 ){
    days = ("0" + days).slice(-2);
  }
  if( hours < 10 ){
    hours = ("0" + hours).slice(-2);
  }
  if( mins < 10 ){
    mins = ("0" + mins).slice(-2);
  }
  if( secs < 10 ){
    secs = ("0" + secs).slice(-2);
  }

  var idEl = document.getElementById(id);
  idEl.getElementsByClassName('days')[0].innerHTML = days + ' : ';
  idEl.getElementsByClassName('hours')[0].innerHTML = hours + ' : ';
  idEl.getElementsByClassName('minutes')[0].innerHTML = mins + ' : ';
  idEl.getElementsByClassName('seconds')[0].innerHTML = secs;

  clearTimeout(countDownToTime.interval);
  countDownToTime.interval = setTimeout(function(){ countDownToTime(countTo, id); },1000);
}